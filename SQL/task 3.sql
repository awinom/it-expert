﻿-- Задача 3

--region CREATE and INSERT
IF OBJECT_ID('Labels') IS NOT NULL
DROP TABLE Labels;

CREATE TABLE Labels
(
  Id BIGINT,          -- первичный ключ таблицы
	Code VARCHAR(20),   -- код записи
	Label VARCHAR(255), -- наименование записи
	Archive BIT         -- логическое поле, являющееся признаком архивности записи
)

INSERT INTO Labels (Id,Code,Label,Archive)
VALUES 
 (1, '0', 'мужской', 0)
,(2, '1', 'женский', 0)
,(3, '2', 'не известен', 1)
,(4, '3', 'не определен', 0)
,(5, '4', NULL, 0)
,(6, NULL, 'женский', 0)
,(7, '10', 'женский', NULL)
--endregion 

------------------------------------- Ответ -----------------------------------------------
---------------------------------- к Заданию 3 --------------------------------------------


-- 3. Необходимо написать один запрос select, результатом которого будет строка типа varchar(max) 
SELECT STUFF((
              SELECT '; ' + IIF(lab.Code IS NULL, 'null', lab.Code) + 
                     ' - ' + IIF(lab.Label IS NULL, 'null', lab.Label) AS [text()]
                FROM Labels lab
               WHERE ISNULL(lab.Archive,0) = 0
              FOR XML PATH (''), TYPE
             ).value('./text()[1]','varchar(max)'), 1,2,'')


