﻿-- Задача 4 (Вызов функции)

--region CREATE and INSERT
IF OBJECT_ID('ClientPayments') IS NOT NULL
  DROP TABLE ClientPayments;

CREATE TABLE ClientPayments
(
  Id BIGINT IDENTITY, -- первичный ключ таблицы
  ClientId BIGINT , -- Id клиента
  Dt DATETIME2(0),  -- дата платежа
  Amount MONEY      -- сумма платежа
)

INSERT INTO ClientPayments (ClientId,Dt,Amount)
VALUES (1,'2022-01-03 17:24:00',100),
       (1,'2022-01-05 17:24:14',200),
       (1,'2022-01-05 18:23:34',250),
       (1,'2022-01-07 10:12:38',50),
       (2,'2022-01-05 17:24:14',278),
       (2,'2022-01-10 12:39:29',300)
--endregion

--------------------------------- Вызов функции -------------------------------------------
---------------------------------- к Заданию 4 --------------------------------------------

DECLARE @startDate DATE = DATEFROMPARTS(2022, 01, 02);
DECLARE @endDate DATE = DATEFROMPARTS(2022, 01, 09);

SELECT * FROM dbo.payments_get_pay_by_client_date (1,@startDate,@endDate)



