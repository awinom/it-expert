﻿-- Задача 2

--region CREATE AND INSERT
IF OBJECT_ID('Dates') IS NOT NULL
DROP TABLE Dates;

CREATE TABLE Dates -- клиенты
(
   Id BIGINT -- NOT NULL добавил бы ограничения на поля
	,Dt DATE   -- NOT NULL 
);

INSERT INTO Dates (Id,Dt)
  VALUES (1,'01.01.2021')
        ,(1,'30.01.2021')
        ,(1,'10.01.2021')
        ,(1,'30.01.2021')
        ,(2,'15.01.2021')
        ,(2,'30.01.2021')
        ,(3,'29.01.2021')
        ,(1,'30.01.2021');
--endregion

------------------------------------- Ответ -----------------------------------------------
---------------------------------- к Заданию 2 --------------------------------------------

-- 2. Написать запрос, который возвращает интервалы для одинаковых Id
WITH Dates_Sort (Id,Dt,row)
AS 
(
  SELECT d.Id,d.Dt, ROW_NUMBER() OVER (PARTITION BY d.Id ORDER BY d.Dt) row
    FROM Dates d 
)


SELECT q.Id,q.Dt Sd, q1.Dt Ed
  FROM Dates_Sort q
  LEFT JOIN Dates_Sort q1 ON q1.Id = q.Id AND q1.row = q.row + 1
  WHERE q1.Dt IS NOT NULL 
  --OR q.row = 1 --если нужно выводить оригинальные Id тоже

