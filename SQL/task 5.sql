﻿-- Задача 5

--region create and insert
IF OBJECT_ID('Clients') IS NOT NULL
  DROP TABLE Clients;

create table Clients
(
Id             int not null identity(1,1), -- Id клиента
ClientName	   nvarchar(255), -- наименование клиента
ReportDayCount int not null default 1 -- кол-во отчетных дней
)

IF OBJECT_ID('ClientDaysSum') IS NOT NULL
  DROP TABLE ClientDaysSum;

create table ClientDaysSum
(
Id    	  int not null identity(1,1), -- первичный ключ таблицы
ClientId	int, -- Id клиента
DayNum  	int, -- порядковый номер отчетного дня
DaySum  	money -- сумма начислений за отчетный день
)


insert into Clients(ClientName, ReportDayCount)
values
  (N'Client1', 5),
  (N'Client2', 14),
  (N'Client3', 7),
  (N'Client4', 3);
 
declare
  @i	int = 1,
  @day  int
 
while @i < 100000 begin
  insert into ClientDaysSum(ClientId, DayNum, DaySum)
  values(floor(rand() * 3) + 1, floor(rand() * 100) + 1, (floor(rand() * 200) + 1)*rand())
 
  set @i = @i + 1
end
--endregion


------------------------------------- Ответ -----------------------------------------------
---------------------------------- к Заданию 5 --------------------------------------------

-- 5. Необходимо написать по крайней мере 2 варианта одиночных select-запроса
SELECT c.ClientName,
       top_sort.DayNum,
       top_sort.DaySum
  FROM Clients c
 OUTER APPLY (SELECT TOP (c.ReportDayCount) 
                     cds.DayNum,
                     cds.DaySum
                FROM Clients c1
                LEFT JOIN ClientDaysSum cds ON cds.ClientId = c1.Id
               WHERE c1.Id = c.Id
               ORDER BY cds.DaySum DESC
             ) top_sort
 ORDER BY 1,3


SELECT c1.ClientName,
       q.DayNum,
       q.DaySum
  FROM Clients c1
  LEFT JOIN (
              SELECT c.Id,
                     cds.DayNum,
                     cds.DaySum,
                     ROW_NUMBER() OVER(PARTITION BY c.Id ORDER BY cds.DaySum DESC) row
                FROM Clients c
                LEFT JOIN ClientDaysSum cds ON cds.ClientId = c.Id
             ) q ON q.Id = c1.Id AND q.row <= c1.ReportDayCount
 ORDER BY 1,3




