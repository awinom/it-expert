﻿-- Задача 4 (Функция)

/**
 * Возвращает суммы платежей по клиенту за указанный интервал
 *
 *
 * @param @ClientId BIGINT  - Идентификатор клиента
 * @param @Sd DATE - Дата с которой собираем платежи по клиенту
 * @param @Ed DATE - Дата по которую собираем платежи по клиенту
 * 
 * @author Городилов Александр Дмитриевич
 * https://hh.ru/resume/22ba637bff0aff97d00039ed1f76536535724b
 * https://t.me/awinom
 */
ALTER FUNCTION dbo.payments_get_pay_by_client_date
(  
  @ClientId BIGINT,
  @Sd DATE, 
  @Ed DATE  
)
RETURNS @result_tbl TABLE (Dt DATE, Сумма MONEY)
AS
BEGIN

  IF (@Sd <= @Ed AND @ClientId IS NOT NULL) 
  BEGIN 
    WITH dates(DATE) AS 
    (
        SELECT @Sd as DATE  
        UNION ALL
        SELECT DATEADD(DAY,1,DATE)
        FROM dates 
        WHERE DATE < @Ed
    )
    
    INSERT INTO @result_tbl
    SELECT d.Date,
           SUM(ISNULL(c.Amount,0))
      FROM dates d
      LEFT JOIN ClientPayments c ON c.ClientId = @ClientId 
       AND CONVERT(DATE,c.Dt) = d.Date
     GROUP BY d.Date
    OPTION (MAXRECURSION 0);
  END 

  ELSE 
  BEGIN 
    INSERT INTO @result_tbl
    VALUES (NULL,NULL) 
  END 

 RETURN;  
END
GO

