﻿-- Задача 1

--region CREATE
IF OBJECT_ID('Clients') IS NOT NULL
DROP TABLE Clients;

IF OBJECT_ID('ClientContacts') IS NOT NULL
DROP TABLE ClientContacts;


CREATE TABLE Clients -- клиенты
(
	Id bigint, -- Id клиента
	ClientName nvarchar(200) -- Наименование клиента
);


CREATE TABLE ClientContacts -- контакты клиентов
(
	Id bigint, -- Id контакта
	ClientId bigint, -- Id клиента
	ContactType nvarchar(255), -- тип контакта
	ContactValue nvarchar(255) --  значение контакта
);
--endregion 

--region CONSTRAINT 
--(в идеале добавил бы минимальные ограничения, предварительно поправив данные в таблицах)
-- также заменил бы ContactType на тип INT и создал бы отдельную таблицу с типами контактов
ALTER TABLE Clients
ALTER COLUMN Id BIGINT NOT NULL; 
GO

ALTER TABLE Clients
ALTER COLUMN ClientName NVARCHAR(200)  NOT NULL; 
GO

ALTER TABLE Clients
ADD CONSTRAINT PK_Clients PRIMARY KEY (Id)
GO

ALTER TABLE ClientContacts
ALTER COLUMN Id BIGINT NOT NULL; 
GO

ALTER TABLE ClientContacts
ALTER COLUMN ContactType NVARCHAR(255) NOT NULL; 
GO

ALTER TABLE ClientContacts
ALTER COLUMN ContactValue NVARCHAR(255) NOT NULL; 
GO

ALTER TABLE ClientContacts
ALTER COLUMN ClientId BIGINT NOT NULL; 
GO

ALTER TABLE ClientContacts
ADD CONSTRAINT PK_ClientContacts PRIMARY KEY (Id)
GO

ALTER TABLE ClientContacts
ADD CONSTRAINT FK_ClientContacts_Reference_Clients FOREIGN KEY (ClientId) REFERENCES Clients (Id) ON DELETE CASCADE
GO

--endregion

--region INTO
INSERT INTO Clients (Id,ClientName)
VALUES (1,N'Сергей З')
      ,(2,N'Дмитрий М')
      ,(4,N'Александр Г')
      ,(7,N'Антон У')
      ,(9,N'Аркаша И')
      ,(0,N'Михаил П')
--      ,(null,N'Гаша З') -- для проверки
--      ,(4,N'Гоша P') -- для проверки
--      ,(5,NULL) -- для проверки


INSERT INTO ClientContacts (Id,ClientId,ContactType,ContactValue)
  VALUES (1,1,N'телефон','759302')
        ,(2,2,N'почта','asas@sadsf.com')
        ,(4,4,N'телефон','734534')
        ,(7,7,N'телефон','73562')
        ,(9,9,N'телефон','7357643')
        ,(10,2,N'телефон','77846756')
        ,(11,1,N'почта',N'asasasd@ssd.com')
        ,(13,4,N'рабочий телефон','1758645342')
--        ,(8,8,N'телефон','74676524') -- для проверки
--        ,(12,3,N'домашний телефон','95846735') -- для проверки
--        ,(12,4,N'домашний телефон','95846735') -- для проверки
--        ,(12,4,N'телефон','asad@ss.vpd') -- для проверки
--        ,(null,4,N'телефон','assdaad@ss.cas') -- для проверки
--        ,(12,null,N'телефон','daad@ss.ru') -- для проверки
--endregion

------------------------------------- Ответ -----------------------------------------------
---------------------------------- к Заданию 1 --------------------------------------------

--1.1 Написать запрос, который возвращает наименование клиентов и кол-во контактов клиентов
SELECT ISNULL(c.ClientName,CONCAT('ОШИБОЧНЫЙ, клиента не существует, либо не указано его имя (id клиента = '
                                  ,COALESCE(con.ClientId,c.Id)
                                  ,IIF(COALESCE(con.ClientId,c.Id) IS NULL,'null',''),')')) [наименование клиента]
      ,ISNULL(con.count,0) [кол-во контактов клиентов]
  FROM Clients c
  FULL JOIN (
             SELECT cc.ClientId
                    ,COUNT(cc.Id) count
               FROM ClientContacts cc
              GROUP BY cc.ClientId
             ) con ON c.Id = con.ClientId

--1.2 Написать запрос, который возвращает список клиентов, у которых есть более 2 контактов
SELECT ISNULL(c.ClientName,CONCAT('ОШИБОЧНЫЙ, клиента не существует, либо не указано его имя (id клиента = '
                                  ,COALESCE(client.ClientId,c.Id)
                                  ,IIF(COALESCE(client.ClientId,c.Id) IS NULL,'null',''),')')) [наименование клиента]
FROM (
SELECT cc.ClientId
       ,ROW_NUMBER() OVER (PARTITION BY cc.ClientId ORDER BY cc.Id) row
  FROM ClientContacts cc) client
  LEFT JOIN Clients c ON c.Id = client.ClientId
  WHERE client.row > 1

